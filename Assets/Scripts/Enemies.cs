﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    [SerializeField] float speed=1;
    private bool direccionHorizontal = false;
    void Update()
    {
        if (!direccionHorizontal)
        {
            if (transform.position.x <= 9 || transform.position.x >= 9)
            {
                Vector3 currentPosition = transform.position;
                currentPosition.x += 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
                direccionHorizontal = true;
            }
                else
                {
                    direccionHorizontal = false;
                }
            }

               else if(transform.position.x >= -9.3)
                {
                    Vector3 currentPosition = transform.position;
                    currentPosition.x += -1 * speed * Time.deltaTime;
                    transform.position = currentPosition;
                }
            }
    }